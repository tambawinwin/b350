-- 1. Find all artists that has letter d in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- 2. Find all songs that has a length of less than 4 minutes.
SELECT * FROM songs WHERE length < 400;

-- 3. Join the 'albums' and 'songs' tables. (Only show the album name, song name, and song length.)
SELECT album_title, song_name, length 
	FROM albums
	JOIN songs ON albums.id = songs.album_id;

-- 4. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name.)
SELECT * 
	FROM artists
	JOIN albums ON artists.id = albums.artist_id
	WHERE album_title LIKE "%a%";

-- 5. Sort the albums in Z-A order. (Show only the first 4 records.)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- 6. Join the 'albums' and 'songs' tables and find all songs greater than 3 minutes 30 seconds. (Sort albums from descending order)
SELECT * 
FROM albums
	JOIN songs ON albums.id = songs.album_id
	WHERE length > 330
	ORDER BY album_title DESC;

-- PRACTICE
-- We want to view the full name of the user, the date and time the playlist was created, along with the song details (title, length, genre), the name of the album it belongs to, and the artist. The table should display the following column names: Full Name, Creation Date, Song, Duration, Genre, Album, Artist.
SELECT 
    full_name AS "Full Name",
    datetime_created AS "Creation Date",
    song_name AS "Song",
    length AS "Duration",
    genre AS "Genre",
    album_title AS "Album",
    name AS "Artist"
FROM playlists
JOIN users ON playlists.user_id = users.id
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id
JOIN albums ON songs.album_id = albums.id
JOIN artists ON albums.artist_id = artists.id;
