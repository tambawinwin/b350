1. List the books Authored by Marjorie Green.
Answer:
	1. title_id BU1032 The Buy Executive's Database Guide
	2. title_id BU2075 You Can Combat Computer Stree!

2. List the books Authored by Michael O'Leary.
Answer:
	1. title_id BU1111 Cooking with Computers
	2. title_id TC7777 

3. Write the author/s of "The Busy Executive’s Database Guide".
Answer:
	1. au_id 213-46-8915 Marjorie Green
	2. au_id 409-56-7008 Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".
Answer:
	1. pub_id 1389 Algodata Infosystems

5. List the books published by Algodata Infosystems.
Answer:
	1. title_id BU1032 The Busy Executive's Database Guide
	2. title_id BU1111 Cooking with Computers
	3. title_id BU7832 Straight Talk About Computers
	4. title_id PC1035 But Is It User Friendly?
	5. title_id PC8888 Secrets of Silicon Valley
	6. title_id PC9999 Net Etiquette